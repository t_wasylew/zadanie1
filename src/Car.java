public class Car {

    private String name;
    private int numberOfSeats;
    private int engineVolume;
    private int horsePower;
    private int currentSpeed;
    private int currentMilage;
    private int productionYear;
    private int passengerNumber;
    private double maxSpeed;

    public Car(String name, int numberOfSeats, int engineVolume, int horsePower, int currentMilage, int productionYear) {
        this.name = name;
        this.numberOfSeats = numberOfSeats;
        this.engineVolume = engineVolume;
        this.horsePower = horsePower;
        this.currentMilage = currentMilage;
        this.productionYear = productionYear;
        maxSpeed();
    }

    public void maxSpeed() {
        if (horsePower <= 200) {
            setMaxSpeed(1.2 * horsePower);
        } else {
            setMaxSpeed(horsePower);
        }
    }
    public void speedIncrese() {
        if (getPassengerNumber() > 0) {
            if (getCurrentSpeed() < getMaxSpeed()) {
                setCurrentSpeed(getCurrentSpeed() + 10);
            }
        }
    }

    public void speedDecrease() {
        if (getPassengerNumber() > 0 ){
            setCurrentSpeed(getCurrentSpeed() - 10);
        }
    }

    public void removePassenger() {
        setPassengerNumber(getPassengerNumber()-1);
    }

    public void addPassenger() {
        setPassengerNumber(getPassengerNumber()+1);
    }

    public String getCarName() {
        return toString();
    }

    @Override
    public String toString() {
        return "Car{" +
                "name='" + name + '\'' +
                ", productionYear=" + productionYear +
                '}';
    }

    public int getCurrentSpeed() {
        return currentSpeed;
    }

    public void setCurrentSpeed(int currentSpeed) {
        this.currentSpeed = currentSpeed;
    }

    public int getCurrentMilage() {
        return currentMilage;
    }

    public void setCurrentMilage(int currentMilage) {
        this.currentMilage = currentMilage;
    }

    public int getPassengerNumber() {
        return passengerNumber;
    }

    public void setPassengerNumber(int passengerNumber) {
        this.passengerNumber = passengerNumber;
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(double maxSpeed) {
        this.maxSpeed = maxSpeed;
    }
}
